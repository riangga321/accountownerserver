﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Models
{
    [Table("owner")]
    public class Owner
    {
        [Key]
        public Guid own_id { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [StringLength(60, ErrorMessage = "Name can't be longer than 60 characters")]
        public string own_name { get; set; }

        [Required(ErrorMessage = "Date of birth is required")]
        public DateTime own_dateofbirth { get; set; }
    }
}
