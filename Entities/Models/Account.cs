﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Models
{
    [Table("account")]
    public class Account
    {
        [Key]
        public Guid acc_id { get; set; }

        [Required(ErrorMessage = "Date created is required")]
        public DateTime acc_datecreated { get; set; }

        [Required(ErrorMessage = "Account type is required")]
        public String acc_type { get; set; }

        [Required(ErrorMessage = "Owner ID is required")]
        public Guid acc_own_id { get; set; }
    }
}
