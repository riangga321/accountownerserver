# ASP .NET with MySQL 

![N|Solid](http://www.decatechlabs.com/wp-content/uploads/2017/11/ASP-NET-Core-Logo-1.png)


## Prerequisites

For this series you will need:

  - MySQL server installed on your machine
  - .NET SDK
  - Visual Studio 2017 / MonoDevelop
